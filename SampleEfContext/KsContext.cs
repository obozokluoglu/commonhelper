﻿/* Auto generated by Ks Code Generator */

using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using Ks.EntityFramework;
using SeturMarinasWeb.Common.Entity;

namespace SeturMarinasWeb.Data
{
    public class KsContext : KsDbContext
    {
        public virtual IDbSet<Activity> Activities { get; set; }
        public virtual IDbSet<Attachment> Attachments { get; set; }
        public virtual IDbSet<AuditLog> AuditLogs { get; set; }
        public virtual IDbSet<Banner> Banners { get; set; }
        public virtual IDbSet<BulletinEmail> BulletinEmails { get; set; }
        public virtual IDbSet<Campaign> Campaigns { get; set; }
        public virtual IDbSet<CampaignTranslation> CampaignTranslations { get; set; }
        public virtual IDbSet<CommercialUnit> CommercialUnits { get; set; }
        public virtual IDbSet<CommercialUnitCategory> CommercialUnitCategories { get; set; }
        public virtual IDbSet<CommercialUnitCategoryTranslation> CommercialUnitCategoryTranslations { get; set; }
        public virtual IDbSet<CommercialUnitTranslation> CommercialUnitTranslations { get; set; }
        public virtual IDbSet<Email> Emails { get; set; }
        public virtual IDbSet<EmailAttachment> EmailAttachments { get; set; }
        public virtual IDbSet<EmailStatus> EmailStatus { get; set; }
        public virtual IDbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual IDbSet<Gallery> Galleries { get; set; }
        public virtual IDbSet<GalleryDetail> GalleryDetails { get; set; }
        public virtual IDbSet<GivenService> GivenServices { get; set; }
        public virtual IDbSet<GivenServiceCategory> GivenServiceCategories { get; set; }
        public virtual IDbSet<GivenServiceCategoryTranslation> GivenServiceCategoryTranslations { get; set; }
        public virtual IDbSet<GivenServiceTranslation> GivenServiceTranslations { get; set; }
        public virtual IDbSet<HelpfulLink> HelpfulLinks { get; set; }
        public virtual IDbSet<HelpfulLinkTranslation> HelpfulLinkTranslations { get; set; }
        public virtual IDbSet<ImportantNumber> ImportantNumbers { get; set; }
        public virtual IDbSet<ImportantNumberTranslation> ImportantNumberTranslations { get; set; }
        public virtual IDbSet<Language> Languages { get; set; }
        public virtual IDbSet<Log> Logs { get; set; }
        public virtual IDbSet<LogService> LogServices { get; set; }
        public virtual IDbSet<Marina> Marinas { get; set; }
        public virtual IDbSet<MarinaActivity> MarinaActivities { get; set; }
        public virtual IDbSet<MarinaActivityPicture> MarinaActivityPictures { get; set; }
        public virtual IDbSet<MarinaActivityTranslation> MarinaActivityTranslations { get; set; }
        public virtual IDbSet<MarinaCampaign> MarinaCampaigns { get; set; }
        public virtual IDbSet<MarinaCommercialUnit> MarinaCommercialUnits { get; set; }
        public virtual IDbSet<MarinaCommercialUnitTranslation> MarinaCommercialUnitTranslations { get; set; }
        public virtual IDbSet<MarinaGivenService> MarinaGivenServices { get; set; }
        public virtual IDbSet<MarinaGivenServiceTranslation> MarinaGivenServiceTranslations { get; set; }
        public virtual IDbSet<MarinaMarinaActivity> MarinaMarinaActivities { get; set; }
        public virtual IDbSet<MarinaMustSeePlace> MarinaMustSeePlaces { get; set; }
        public virtual IDbSet<MarinaNewsAnnouncementCampaign> MarinaNewsAnnouncementCampaigns { get; set; }
        public virtual IDbSet<MarinaPicture> MarinaPictures { get; set; }
        public virtual IDbSet<MarinaTranslation> MarinaTranslations { get; set; }
        public virtual IDbSet<MarinaUsefulNumber> MarinaUsefulNumbers { get; set; }
        public virtual IDbSet<Menu> Menus { get; set; }
        public virtual IDbSet<MustSeePlace> MustSeePlaces { get; set; }
        public virtual IDbSet<MustSeePlacesPicture> MustSeePlacesPictures { get; set; }
        public virtual IDbSet<MustSeePlaceTranslation> MustSeePlaceTranslations { get; set; }
        public virtual IDbSet<NewsAnnouncementCampaign> NewsAnnouncementCampaigns { get; set; }
        public virtual IDbSet<NewsAnnouncementsCampaignTranslation> NewsAnnouncementsCampaignTranslations { get; set; }
        public virtual IDbSet<NonWorkingDay> NonWorkingDays { get; set; }
        public virtual IDbSet<OfferCalculation> OfferCalculations { get; set; }
        public virtual IDbSet<PageContent> PageContents { get; set; }
        public virtual IDbSet<PageContentTranslation> PageContentTranslations { get; set; }
        public virtual IDbSet<Permission> Permissions { get; set; }
        public virtual IDbSet<Queue> Queues { get; set; }
        public virtual IDbSet<QueueError> QueueErrors { get; set; }
        public virtual IDbSet<QueueProcessing> QueueProcessings { get; set; }
        public virtual IDbSet<QueueSuccess> QueueSuccesses { get; set; }
        public virtual IDbSet<QueueType> QueueTypes { get; set; }
        public virtual IDbSet<Reservation> Reservations { get; set; }
        public virtual IDbSet<Role> Roles { get; set; }
        public virtual IDbSet<RolePermission> RolePermissions { get; set; }
        public virtual IDbSet<Schedule> Schedules { get; set; }
        public virtual IDbSet<ScheduleFrequencyType> ScheduleFrequencyTypes { get; set; }
        public virtual IDbSet<ScheduleStatus> ScheduleStatus { get; set; }
        public virtual IDbSet<Setting> Settings { get; set; }
        public virtual IDbSet<SettingScope> SettingScopes { get; set; }
        public virtual IDbSet<SettingType> SettingTypes { get; set; }
        public virtual IDbSet<UsefulNumber> UsefulNumbers { get; set; }
        public virtual IDbSet<UsefulNumberTranslation> UsefulNumberTranslations { get; set; }
        public virtual IDbSet<UserDelegation> UserDelegations { get; set; }
        public virtual IDbSet<UserLogin> UserLogins { get; set; }
        public virtual IDbSet<UserPasswordHistory> UserPasswordHistories { get; set; }
        public virtual IDbSet<UserRole> UserRoles { get; set; }
        public virtual IDbSet<User> Users { get; set; }
        public virtual IDbSet<WfAssignmentType> WfAssignmentTypes { get; set; }
        public virtual IDbSet<WfDefinition> WfDefinitions { get; set; }
        public virtual IDbSet<WfDefinitionExpiration> WfDefinitionExpirations { get; set; }
        public virtual IDbSet<WfEmailCommand> WfEmailCommands { get; set; }
        public virtual IDbSet<WfExpiration> WfExpirations { get; set; }
        public virtual IDbSet<WfExpirationFrequencyType> WfExpirationFrequencyTypes { get; set; }
        public virtual IDbSet<WfInstance> WfInstances { get; set; }
        public virtual IDbSet<WfPool> WfPools { get; set; }
        public virtual IDbSet<WfStatus> WfStatus { get; set; }
        public virtual IDbSet<WfStepAssignment> WfStepAssignments { get; set; }
        public virtual IDbSet<WfStepDefinition> WfStepDefinitions { get; set; }
        public virtual IDbSet<WfStepDefinitionExpiration> WfStepDefinitionExpirations { get; set; }
        public virtual IDbSet<WfStepDirection> WfStepDirections { get; set; }
        public virtual IDbSet<WfStepInstance> WfStepInstances { get; set; }
        public virtual IDbSet<WfStepStatus> WfStepStatus { get; set; }
        public virtual IDbSet<YatchRally> YatchRallies { get; set; }
        public virtual IDbSet<YatchRallySponsor> YatchRallySponsors { get; set; }
        public virtual IDbSet<YatchRallyTranslation> YatchRallyTranslations { get; set; }

        public KsContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by Ks to pass connection string defined in KsDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of KsContext since Ks automatically handles it.
         */

        public KsContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            SetSchemaName(modelBuilder);
            RegisterEntityTypeConfigurations(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void SetSchemaName(DbModelBuilder modelBuilder)
        {
            var connectionString = Database.Connection.ConnectionString;

            if (!connectionString.ToLowerInvariant().Contains("database="))
            {
                var builder = new DbConnectionStringBuilder { ConnectionString = connectionString };
                var schema = (string)builder["user id"];
                modelBuilder.HasDefaultSchema(schema);
            }
        }

        private void RegisterEntityTypeConfigurations(DbModelBuilder modelBuilder)
        {
            var connectionString = Database.Connection.ConnectionString;

            if (!connectionString.ToLowerInvariant().Contains("database="))
            {
                var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                    .Where(type => !String.IsNullOrEmpty(type.Namespace))
                    .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                                   && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
                foreach (var type in typesToRegister)
                {
                    dynamic configurationInstance = Activator.CreateInstance(type);
                    modelBuilder.Configurations.Add(configurationInstance);
                }
            }
        }

    }
}