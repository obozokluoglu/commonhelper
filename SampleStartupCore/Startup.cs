﻿using System.Linq;
using AspNetCoreRateLimit;
using DealerAutonom.Conventions;
using DealerAutonom.DBContext;
using DealerAutonom.Domain;
using DealerAutonom.DomainService;
using DealerAutonom.DomainService.Contract;
using DealerAutonom.Helpers;
using DealerAutonom.Helpers.Mapping;
using DealerAutonom.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;


namespace DealerAutonom
{
    public class Startup
    {
        public static IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
            });

            //services.AddApiVersioning();

            services.AddMvc(setupAction =>
            {

                //setupAction.Conventions.Add(new ApiExplorerGroupPerVersionConvention());

                setupAction.ReturnHttpNotAcceptable = true;
                setupAction.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                setupAction.InputFormatters.Add(new XmlDataContractSerializerInputFormatter());

                var xmlDataContractSerializerInputFormatter =
                new XmlDataContractSerializerInputFormatter();
                setupAction.InputFormatters.Add(xmlDataContractSerializerInputFormatter);

                var jsonInputFormatter = setupAction.InputFormatters
                .OfType<JsonInputFormatter>().FirstOrDefault();

                if (jsonInputFormatter != null)
                {
                    jsonInputFormatter.SupportedMediaTypes.Add("application/vnd.marvin.dealer.full+json");

                }

                var jsonOutputFormatter = setupAction.OutputFormatters.OfType<JsonOutputFormatter>().FirstOrDefault();

                if (jsonOutputFormatter != null)
                {
                    jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.marvin.hateoas+json");
                }





            })

            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            var connectionString = Configuration["connectionStrings:dealerDBConnectionString"];
            services.AddDbContext<DealerContext>(o => o.UseSqlServer(connectionString));

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IUrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });


            services.AddTransient<IDealerService, DealerService>();

            services.AddTransient<IPropertyMappingService, PropertyMappingService>();

            services.AddTransient<ITypeHelperService, TypeHelperService>();

            services.AddHttpCacheHeaders(
                (expirationModelOptions)
                =>
                {
                    expirationModelOptions.MaxAge = 600;
                },
                (validationModelOptions)
                =>
                {
                    validationModelOptions.AddMustRevalidate = true;
                });

            services.AddMemoryCache();

            services.Configure<IpRateLimitOptions>((options) =>
            {
                options.GeneralRules = new System.Collections.Generic.List<RateLimitRule>()
                {
                    new RateLimitRule()
                    {
                        Endpoint = "*",
                        Limit = 1000,
                        Period = "5m"
                    },
                    new RateLimitRule()
                    {
                        Endpoint = "*",
                        Limit = 300,
                        Period = "10s"
                    }
                };
            });

            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Dealer Autonom - V1",
                    Version = "v1",
                    Description = "Simple Dealer Management Microservice",
                    TermsOfService = "",
                    Contact = new Contact
                    {
                        Name = "GhostBusters",
                        Email = "GhostBusters@kocsistem.com.tr"
                    },
                    License = new License
                    {
                        Name = "OneFrame",
                        Url = "http://confluence.kocsistem.com.tr/display/KSFRAM/OneFrame"
                    }
                });
            });


        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory, DealerContext libraryContext)
        {
            var logger = loggerFactory.CreateLogger("GlobalLogger");

            logger.LogInformation("Application configuration starting...");
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                app.UseHttpStatusCodeExceptionMiddleware();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {

                            logger.LogError(500,
                                exceptionHandlerFeature.Error,
                                exceptionHandlerFeature.Error.Message);
                        }

                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected fault happened. Try again later.");

                    });
                });

                app.UseHttpStatusCodeExceptionMiddleware();
            }

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Dealer, Models.DealerDto>();

                cfg.CreateMap<User, Models.UserDto>();

                cfg.CreateMap<Models.DealerForCreationDto, Dealer>();

                cfg.CreateMap<Models.UserForCreationDto, User>();

                cfg.CreateMap<Models.UserForUpdateDto, User>();

                cfg.CreateMap<User, Models.UserForUpdateDto>();
            });

            libraryContext.EnsureSeedDataForContext();

            app.UseIpRateLimiting();

            app.UseHttpCacheHeaders();

            app.UseMvc();

            app.UseStaticFiles();

            app.UseSwagger();

            app.UseCors("AllowAll");


            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dealer Autonom API V1");
                c.DocumentTitle = "Dealer Autonom DT";

                c.DisplayRequestDuration();
                c.DocExpansion(DocExpansion.List);
                c.EnableFilter();
                c.ShowExtensions();
                c.EnableValidator();
                c.DefaultModelRendering(ModelRendering.Model);

            });

            logger.LogInformation("Application has been property configured !");
        }
    }
}
