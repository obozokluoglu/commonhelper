﻿using System.Data;

namespace Common.AdoNet
{
    public class DbQueryParameter
    {
        public string Name { get; set; }
        public ParameterDirection Direction { get; set; }
        public object Value { get; set; }

        public DbQueryParameter(string paramName, object paramValue, ParameterDirection paramDirection = ParameterDirection.Input)
        {
            Name = paramName;
            Direction = paramDirection;
            Value = paramValue;
        }
    }
}