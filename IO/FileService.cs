﻿using System;
using System.IO;
using System.Web;
using Abp;

namespace DSystem.IO
{
    public class FileService
    {
        public static string SaveFile(string path, HttpPostedFileBase file)
        {
            if (Directory.Exists(path))
            {
                string fileName = file.FileName;
                if (!File.Exists(path + fileName))
                {
                    file.SaveAs(path + fileName);
                    return file.FileName;
                }
                fileName = file.FileName.Replace(Path.GetExtension(fileName), "_0");
                fileName += Path.GetExtension(file.FileName);
                file.SaveAs(path + fileName);
                return fileName;
            }
            throw new AbpException("Dosya yolu bulunamadı:" + path);
        }

        public static void RemoveFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            else
            {
                throw new AbpException("Dosya yolu bulunamadı:" + path);
            }

        }
    }
}
