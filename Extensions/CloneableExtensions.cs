﻿using System;
using Ks.Common.Interface;

namespace Ks.Utility.Extension
{
    /// <summary>
    /// CloneableExtensions
    /// </summary>
    public static class CloneableExtensions
    {
        /// <summary>
        /// Clones object U to T.
        /// </summary>
        /// <typeparam name="T">Targeet object</typeparam>
        /// <param name="u">Source object</param>
        /// <returns>Returns T</returns>
        public static T CloneTo<T>(this ICloneableType u) where T : ICloneableType, new()
        {
            var props = u.GetType().GetProperties();
            T destination = new T();
            foreach (var prop in props)
            {
                var info = destination.GetType().GetProperty(prop.Name);
                var value = prop.GetValue(u, null);

                if (info != null)
                {
                    object underlyingType = Nullable.GetUnderlyingType(info.PropertyType);
                    if (underlyingType != null && value != null)
                    {
                        value = Convert.ChangeType(value, Nullable.GetUnderlyingType(info.PropertyType));
                    }
                }

                info?.SetValue(destination, value, null);
            }
            return destination;
        }
    }
}