﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Ks.Utility.Extension
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return enumValue.ToString();
        }

        public static List<T> GetList<T>(this T _enum)
        {
            Type t = typeof(T);
            return !t.IsEnum ? null : Enum.GetValues(t).Cast<T>().ToList();
        }

        public static List<SelectListItem> GetEnumsForDescriptions(this Type enumObj, int? selected = null)
        {
            Array items = Enum.GetValues(enumObj);
            var result = (from object a in items
                          select new SelectListItem
                          {
                              Selected = selected != null && selected.Value == (int)a,
                              Value = Convert.ToInt32(a).ToString(),
                              Text = enumObj.GetField(a.ToString()).GetCustomAttribute<DescriptionAttribute>().Description
                          }).ToList();
            return result;
        }

        public static string GetEnumDescriptionByEnum(this Enum enumObj)
        {
            if (enumObj != null)
            {
                var type = enumObj.GetType();
                string name = Enum.GetName(type, enumObj);
                return type.GetField(name).GetCustomAttribute<DescriptionAttribute>().Description;
            }
            return string.Empty;
        }
        /// <summary>
        /// Returns the detail of an enum.
        /// </summary>
        /// <typeparam name="T">Enumeration type.</typeparam>
        /// <param name="value">Value of the enum.</param>
        /// <returns></returns>
        public static string GetDetail<T>(object value)
        {
            if (value != null && !String.IsNullOrEmpty(value.ToString()))
            {
                return GetDetail<T>(Convert.ToInt32(value));
            }
            else
            {
                return String.Empty;
            }
        }
        /// <summary>
        /// Returns the detail of an enum.
        /// </summary>
        /// <typeparam name="T">Enumeration type.</typeparam>
        /// <param name="value">Value of the enum.</param>
        /// <returns></returns>
        public static string GetDetail<T>(short value)
        {
            return GetDetail<T>(Convert.ToInt32(value));
        }
        public static string GetDetail<T>(int value)
        {
            foreach (FieldInfo field in typeof(T).GetFields(BindingFlags.Static | BindingFlags.GetField | BindingFlags.Public))
            {
                if (field.GetValue(null).ToString() == Enum.GetName(typeof(T), value))
                {
                    foreach (Attribute attrib in field.GetCustomAttributes(true))
                    {
                        DescriptionAttribute detail = (DescriptionAttribute)attrib;
                        return detail.Description;
                    }
                }
            }

            return String.Empty;
        }
    }
}