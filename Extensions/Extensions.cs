﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Common.Extensions
{
    public static class Extensions
    {
        #region Object

        /// <summary>
        /// Generic sınıfı tipe dönüşüm yapar
        /// </summary>
        public static T As<T>(this object obj)
            where T : class
        {
            return (T) obj;
        }

        /// <summary>
        /// Generic struct tipi sınıfa dönüştürür
        /// </summary>
        public static T To<T>(this object obj)
            where T : struct
        {
            return (T) Convert.ChangeType(obj, typeof(T), CultureInfo.InvariantCulture);
        }

        #endregion

        #region Collection

        public static bool IsNullOrEmpty<T>(this ICollection<T> source)
        {
            return source == null || source.Count <= 0;
        }

        /// <summary>
        /// Filters a <see cref="IEnumerable{T}"/> by given predicate if given condition is true.
        /// </summary>
        /// <param name="source">Enumerable to apply filtering</param>
        /// <param name="condition">A boolean value</param>
        /// <param name="predicate">Predicate to filter the enumerable</param>
        /// <returns>Filtered or not filtered enumerable based on <see cref="condition"/></returns>
        public static IEnumerable<T> WhereIf<T>(this IEnumerable<T> source, bool condition, Func<T, bool> predicate)
        {
            return condition
                ? source.Where(predicate)
                : source;
        }

        /// <summary>
        /// Filters a <see cref="IEnumerable{T}"/> by given predicate if given condition is true.
        /// </summary>
        /// <param name="source">Enumerable to apply filtering</param>
        /// <param name="condition">A boolean value</param>
        /// <param name="predicate">Predicate to filter the enumerable</param>
        /// <returns>Filtered or not filtered enumerable based on <see cref="condition"/></returns>
        public static IEnumerable<T> WhereIf<T>(this IEnumerable<T> source, bool condition, Func<T, int, bool> predicate)
        {
            return condition
                ? source.Where(predicate)
                : source;
        }

        #endregion

        #region Combobox

        public static MultiSelectList ToMultiSelectList(this IEnumerable collection, IEnumerable selectedValues = null)
        {
            return new MultiSelectList(collection, "Value", "Text", selectedValues);
        }

        public static SelectList ToSelectList(this IEnumerable collection)
        {
            return ToSelectList(collection, null);
        }

        public static SelectList ToSelectList(this IEnumerable collection, object selectedValue)
        {
            return new SelectList(collection, "Value", "Text", selectedValue);
        }

        #endregion

        #region Datetime

        /// <summary>
        /// Yaş hesaplaması yapılır
        /// </summary>
        /// <param name="birthDate"></param>
        /// <returns></returns>
        public static int? GetAge(this DateTime birthDate)
        {
            var today = DateTime.Today;

            if (birthDate.Date > today)
            {
                return null;
            }

            var age = today.Year - birthDate.Year;

            if (birthDate > today.AddYears(-age))
            {
                age--;
            }

            return age;
        }

        public static DateTime UnixTimeStampToDateTime(string unixTimeStamp)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(unixTimeStamp)).ToLocalTime();
            return dtDateTime;
        }

        #endregion

        #region Enum

        public static int ToInt(this Enum e)
        {
            return Convert.ToInt32(e);
        }

        /// <summary>
        /// Enum değerlerini listview'e doldurur
        /// </summary>
        /// <param name="enumObj"></param>
        /// <param name="selected">Seçili value değeri</param>
        public static SelectList GetEnumsForDescriptions(this Type enumObj, int? selected = null)
        {
            Array items = Enum.GetValues(enumObj);
            List<SelectListItem> returnList = new List<SelectListItem>();

            foreach (var item in items)
            {
                var newItem = new SelectListItem()
                {
                    Selected = selected != null && selected.Value == (int) item,
                    Value = Convert.ToInt32(item).ToString()
                };

                try
                {
                    newItem.Text =
                        enumObj.GetField(item.ToString()).GetCustomAttribute<DescriptionAttribute>().Description;
                }
                catch (Exception)
                {
                    newItem.Text = enumObj.GetField(item.ToString()).Name;
                }

                returnList.Add(newItem);
            }

            SelectList selectList = new SelectList(returnList, "Value", "Selected");
            return selectList;
        }

        /// <summary>
        /// Enum'a ait DescriptionAttribute değerini getirir
        /// </summary>
        /// <param name="enumObj"></param>
        /// <returns></returns>
        public static string GetEnumDescriptionByEnum(this Enum enumObj)
        {
            if (enumObj != null)
            {
                var type = enumObj.GetType();
                string name = Enum.GetName(type, enumObj);
                return type.GetField(name).GetCustomAttribute<DescriptionAttribute>().Description;
            }
            return string.Empty;
        }

        #endregion

        #region IO

        /// <summary>
        /// Creates a new directory if it does not exists.
        /// </summary>
        /// <param name="directory">Directory to create</param>
        public static void CreateIfNotExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        /// <summary>
        /// Checks and deletes given file if it does exists.
        /// </summary>
        /// <param name="filePath">Path of the file</param>
        public static void DeleteIfExists(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        #endregion

        #region Number

        /// <summary>
        /// Yüzde hesaplaması yapıyor
        /// </summary>
        /// <param name="value"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static double Percentage(this int value, int total)
        {
            if (total.IsDefault())
            {
                return 0;
            }

            if (value.IsDefault())
            {
                return 100;
            }

            return (double) value * 100 / total;
        }

        /// <summary>
        /// Anaparaya kdv ekler
        /// </summary>
        /// <param name="price"></param>
        /// <param name="percentage"></param>
        /// <returns></returns>
        public static decimal ApplyKdv(this decimal price, int percentage = 18)
        {
            if (price <= 0)
            {
                return 0;
            }

            return Math.Round(price + (price * (percentage / 100M)),
                MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// İfade integer mi kontrol eder
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool IsInt(this object source)
        {
            if (source == null) return false;
            int value;
            return int.TryParse(source.ToString(), out value);

        }

        public static bool IsDefault(this int number)
        {
            return number == default(int);
        }

        #endregion

        #region String

        public static bool IsNullOrWhiteSpace(this string val)
        {
            return string.IsNullOrWhiteSpace(val);
        }

        public static string ReplaceTurkishCharacters(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                char[] oldValue = new[] {'ı', 'ş', 'ğ', 'ö', 'ç', 'ü', 'İ', 'Ş', 'Ğ', 'Ö', 'Ç', 'Ü'};
                char[] newValue = new[] {'i', 's', 'g', 'o', 'c', 'u', 'i', 's', 'g', 'o', 'c', 'u'};
                for (int i = 0; i < oldValue.Length; i++)
                {
                    str = str.Replace(oldValue[i], newValue[i]);
                }
            }
            return str;
        }

        public static string IsoDateTimeConverter(this string dateString)
        {
            string json = JsonConvert.SerializeObject(dateString,
                new IsoDateTimeConverter() {DateTimeFormat = "yyyy-MM-dd HH:mm:ss"});
            return json;
        }

        public static string HtmlDecode(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return new Regex(@"<[^\/>]+>[ \n\r\t]*<\/[^>]+>").Replace(
                HttpUtility.HtmlDecode(input.Replace("&nbsp;", " ")), string.Empty);
        }

        public static T ToEnum<T>(this string value)
            where T : struct
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            return (T) Enum.Parse(typeof(T), value);
        }

        /// <summary>
        /// Stringi verilen uzunluk kadar keser
        /// </summary>
        /// <param name="str"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string Left(this string str, int len)
        {
            if (str == null)
                throw new ArgumentNullException("str");

            if (str.Length < len)
                throw new ArgumentException("len argument can not be bigger than given string's length!");

            return str.Substring(0, len);
        }

        /// <summary>
        /// String ifade max uzunluğu geçerse verilen uzunluk kadar keser
        /// </summary>
        /// <param name="str"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string Truncate(this string str, int maxLength)
        {
            if (str == null)
                return null;

            if (str.Length <= maxLength)
                return str;

            return str.Left(maxLength);
        }

        public static string TruncateWithPostfix(this string str, int maxLength)
        {
            return TruncateWithPostfix(str, maxLength, "...");
        }

        public static string TruncateWithPostfix(this string str, int maxLength, string postfix)
        {
            if (str == null)
                return null;

            if (str == string.Empty || maxLength == 0)
                return string.Empty;

            if (str.Length <= maxLength)
                return str;

            if (maxLength <= postfix.Length)
                return postfix.Left(maxLength);

            return str.Left(maxLength - postfix.Length) + postfix;
        }

        #endregion

        #region Converting

        public static bool ToBool(this object value)
        {
            if (value == null)
                return false;

            return value.ToString().Trim().Equals("1", StringComparison.InvariantCulture) ||
                   value.ToString().Trim().Equals("TRUE", StringComparison.InvariantCultureIgnoreCase);
        }

        public static string ToBoolChar(this object value)
        {
            return value.ToBool() ? "1" : "0";
        }

        public static string ToStringWithNullControl(this object value)
        {
            if (value == DBNull.Value || value == null)
            {
                return string.Empty;
            }

            if (value.ToString().Trim() == "null")
            {
                return string.Empty;
            }

            return value.ToString();
        }

        public static int ToInt(this object value)
        {
            int retVal = 0;

            if (string.IsNullOrEmpty(value?.ToString().Trim()))
            {
                return retVal;
            }

            if (value.GetType().BaseType == typeof(Enum))
            {
                return (int) value;
            }

            switch (value.ToString().Trim().ToUpper())
            {
                case "TRUE":
                    return 1;
                case "FALSE":
                    return 0;
            }

            string valueString = value.ToString();

            string seperator = System.Threading.Thread.CurrentThread.CurrentCulture.Name == "tr-TR" ? "," : ".";
            int indexOfSep = valueString.IndexOf(seperator);

            if (indexOfSep != -1)
            {
                valueString = valueString.Substring(0, indexOfSep);
            }

            int.TryParse(valueString, out retVal);

            return retVal;
        }

        public static string ToIntThenToString(this object value)
        {
            return value.ToInt().ToString();
        }

        public static long ToLong(this object valueObj)
        {
            string valueStr = valueObj?.ToString();
            if (string.IsNullOrEmpty(valueStr))
            {
                return 0;
            }

            if (valueObj is Int64)
            {
                return (long) valueObj;
            }

            long value = 0;
            if (long.TryParse(valueStr, out value))
            {
                return value;
            }

            return 0;
        }

        public static string ToLongThenToString(this object value)
        {
            return value.ToLong().ToString();
        }

        public static decimal ToDecimal(this object valueObj)
        {

            if (valueObj == null)
            {
                return 0;
            }
            if (valueObj is decimal)
            {
                return (decimal) valueObj;
            }

            string valueStr = valueObj.ToString();

            if (string.IsNullOrWhiteSpace(valueStr))
            {
                return 0;
            }

            decimal value;
            decimal.TryParse(FormatDecimalString(valueStr), out value);
            return value;
        }

        public static float ToFloat(this object valueObj)
        {
            if (valueObj == null)
            {
                return 0;
            }
            if (valueObj is float)
            {
                return (float) valueObj;
            }

            string valueStr = valueObj.ToString();

            if (string.IsNullOrWhiteSpace(valueStr))
            {
                return 0;
            }

            float value;
            float.TryParse(FormatDecimalString(valueStr), out value);
            return value;
        }


        public static string FormatDecimalString(string value)
        {
            string strValue = value;

            if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "tr-TR")
            {
                strValue = strValue.Replace(".", ",");
            }
            else if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "en-US")
            {
                strValue = strValue.Replace(",", ".");
            }
            else
            {
                throw new NotImplementedException();
            }

            return strValue;
        }

        public static double ToDouble(this object valueObj)
        {
            string valueStr = valueObj?.ToString();
            if (string.IsNullOrEmpty(valueStr))
            {
                return 0;
            }

            if (valueObj is System.Double)
            {
                return (Double) valueObj;
            }

            double value = default(double);

            double.TryParse(FormatDecimalString(valueStr), out value);

            return value;
        }

        public static DateTime ToDateTime(this object value)
        {
            if (value == null || value == DBNull.Value)
            {
                return DateTime.MinValue;
            }

            return Convert.ToDateTime(value);
        }

        public static object ToDbNull(this object obj)
        {
            if (obj == null) return DBNull.Value;

            if (obj is String)
            {
                if (obj.ToString().Trim().Equals(String.Empty)) return DBNull.Value;
            }
            if (obj is Int16 || obj is Int32 || obj is Int64)
            {
                if (obj.ToString().Equals("0")) return DBNull.Value;
            }
            if (obj is Double)
            {
                if (Convert.ToDouble(obj) == 0.0) return DBNull.Value;
            }
            if (obj is Decimal && (Convert.ToDecimal(obj) == (decimal) 0))
            {
                return DBNull.Value;
            }
            if (obj is DateTime)
            {
                if (((DateTime) obj).Equals(DateTime.MinValue))
                    return DBNull.Value;
            }
            return obj;
        }

        public static string ToDateTimeString(this DateTime date)
        {
            if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "tr-TR")
            {
                return date.ToString("dd.MM.yyyy HH:mm");
            }
            else
            {
                return date.ToString("MM.dd.yyyy HH:mm");
            }
        }

        #endregion

        #region Formating

        public static string FormatDoubleValue(this double value, int digitCount)
        {
            string format = "n" + digitCount.ToString();
            return value.ToString(format);
        }

        public static string FormatDecimalValue(this decimal value, int digitCount)
        {
            string format = "n" + digitCount.ToString();
            return value.ToString(format);
        }

        public static string GetMoneyStringValue(this double amount)
        {
            return amount.ToString("#,##.#0");
        }

        #endregion

        #region Json

        public static T FromJsonString<T>(this string jsonStr)
        {
            return JsonConvert.DeserializeObject<T>(jsonStr.ToStringWithNullControl());
        }

        #endregion

        public static string ToStringJoinWithSeperator<T>(this IEnumerable<T> source, string seperator)
        {
            return string.Join(seperator, source.Select(s => s.ToString()).ToArray());
        }

    }
}