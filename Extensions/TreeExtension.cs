﻿using System;
using System.Collections.Generic;
using System.Linq;
using SeturMarinasWeb.Common.Dto.Tree;

namespace SeturMarinasWeb.Utility.Extension
{
    public static class TreeExtension
    {
        public static List<TreeItemModel> ConvertToTree(this List<TreeItemModel> list)
        {
            var treeList = new List<TreeItemModel>();
            list = list.OrderBy(i => i.Id).ToList();

            foreach (var o in list)
            {
                var subItem = treeList.FirstOrDefaultFromMany(p => p.Children, p => p.Id == o.ParentId);
                if (subItem != null || treeList.Any(a => a.Id == o.ParentId))
                {
                    if (subItem != null)
                        subItem.Children.Add(o);
                    else
                    {
                        var firstOrDefault = treeList.FirstOrDefault(a => a.Id == o.ParentId);
                        if (firstOrDefault != null)
                            firstOrDefault.Children.Add(o);
                    }
                }
                else
                    treeList.Add(o);
            }
            return treeList;
        }

        public static T FirstOrDefaultFromMany<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> childrenSelector, Predicate<T> condition)
        {
            if (source == null || !source.Any()) return default(T);

            var attempt = source.FirstOrDefault(t => condition(t));
            if (!Equals(attempt, default(T))) return attempt;

            return source.SelectMany(childrenSelector)
                .FirstOrDefaultFromMany(childrenSelector, condition);
        }
    }
}