﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace Common.Importer
{
    public class CsvImport
    {
        public void Dispose()
        {
        }

        public DataTable Read(string filePath, bool firstRowIsHeader = false, List<Type> columnTypes = null, List<string> columnNames = null)
        {
            var result = new DataTable("Result");

            using (var sr = new StreamReader(filePath, Encoding.GetEncoding("iso-8859-9")))
            {
                string rowTxt = "";

                if (sr.Peek() > -1)
                {
                    rowTxt = sr.ReadLine();
                    if (rowTxt.EndsWith(";"))
                    {
                        rowTxt = rowTxt.Remove(rowTxt.Length - 1);
                    }
                    var values = rowTxt.Split(';');

                    DataRow row = null;

                    if (!firstRowIsHeader)
                    {
                        row = result.NewRow();
                    }
                    for (int i = 0; i < values.Length; i++)
                    {
                        var colName = columnNames != null && columnNames.Any() ? columnNames[i] : string.Format("Column{0}", i);
                        var colType = columnTypes != null && columnTypes.Any() ? columnTypes[i] : typeof(string);

                        var col = new DataColumn(colName, colType);
                        result.Columns.Add(col);

                        if (!firstRowIsHeader)
                        {
                            row[col] = values[i] == null ? null : Convert.ChangeType(values[i], colType);
                        }
                    }
                    if (!firstRowIsHeader)
                    {
                        result.Rows.Add(row);
                    }
                }

                var isStandartColumns = result.Columns.Cast<DataColumn>().Any(c => c.DataType == typeof(string));

                while (sr.Peek() > -1)
                {
                    rowTxt = sr.ReadLine();
                    if (rowTxt.EndsWith(";"))
                    {
                        rowTxt = rowTxt.Remove(rowTxt.Length - 1);
                    }

                    var values = rowTxt.Split(';');

                    if (isStandartColumns)
                    {
                        result.LoadDataRow(values, true);
                    }
                    else
                    {
                        var row = result.NewRow();

                        for (int i = 0; i < result.Columns.Count; i++)
                        {
                            row[result.Columns[i]] = values[i] == null ? null : Convert.ChangeType(values[i], columnTypes[i]);
                        }

                        result.Rows.Add(row);
                    }
                }
            }

            return result;
        }
    }
}