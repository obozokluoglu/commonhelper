﻿using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;

namespace Common.Importer
{
    public class ExcelImport
    {
        public void Dispose()
        {            
        }

        public DataSet Read(string filePath)
        {
            var targetDataSet = new DataSet();
            var sheetNames = ReadSheetNames(filePath);
            var connection = CreateConnection(filePath);
            connection.Open();
            var command = new OleDbCommand { Connection = connection };
            sheetNames.ForEach(
                sheetName =>
                {
                    command.CommandText = "SELECT * FROM [" + sheetName + "$]";
                    try
                    {
                        var dataTable = new DataTable(sheetName);
                        dataTable.Load(command.ExecuteReader());

                        targetDataSet.Tables.Add(dataTable);
                    }
                    catch
                    { }
                });
            connection.Close();
            connection.Dispose();

            return targetDataSet;
        }

        public List<string> ReadSheetNames(string filePath)
        {
            var connection = CreateConnection(filePath);
            connection.Open();
            var schema = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            var sheetNames = new List<string>();
            schema.Rows.Cast<DataRow>().ToList()
                .ForEach(row =>
                {
                    var sheetName = row["TABLE_NAME"].ToString();
                    sheetName = sheetName.Replace("'", "");
                    sheetName = sheetName.Substring(0, sheetName.Length - 1);

                    sheetNames.Add(sheetName);
                });

            connection.Close();
            connection.Dispose();

            return sheetNames;
        }

        private OleDbConnection CreateConnection(string filePath)
        {
            var connectionString = "";
            var fileExtension = filePath.Substring(filePath.LastIndexOf(".") + 1).ToLower();

            switch (fileExtension)
            {
                case "xls":
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
                    break;
                case "xlsx":
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Excel 12.0;";
                    break;
                case "csv":
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Text;";
                    break;
            }
            return new OleDbConnection(connectionString);
        }

    }
}
