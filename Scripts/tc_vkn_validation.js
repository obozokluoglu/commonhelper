﻿(function ($) {

    $.fn.tcknDogrula = function () {
        var tcno = this.val();


        var odd = 0, even = 0, ad = 0, h1 = 0, h2 = 0, curentDecimal, reg = /^\d+$/;
        //T.C. kimlik numaraları 11 hanedir ve tamamı rakamsal değerlerden oluşur.

        if (!tcno.match(reg)) {
            return false;
        }
        if (tcno.charAt(0) === 0 || tcno.length !== 11) {
            return false;
        }


        h1 = parseInt(tcno.charAt(9), 10); //10 haneyi 10 luk tabana çevir
        h2 = parseInt(tcno.charAt(10), 10);
        //1. 3. 5. 7. ve 9. hanelerin toplamının 7 ile çarpımından 2. 4. 6. ve 8. haneler çıkartıldığında geriye kalan sayının 10’a göre modu bize 10. haneyi verir.
        for (var i = 0; i < 9; i++) {
            curentDecimal = parseInt(tcno.charAt(i), 10);
            (i % 2 === 0) ? odd += curentDecimal : even += curentDecimal;
            ad += curentDecimal;
        }

        return (((((7 * odd) - even) % 10) === h1) && ((ad + h1) % 10 === h2)) ? true : false;
    };

})(jQuery);

(function ($) {
    $.fn.vknDogrula = function () {
        var v1 = 0, v2 = 0, v3 = 0, v4 = 0, v5 = 0, v6 = 0, v7 = 0, v8 = 0, v9 = 0,
        v11 = 0, v22 = 0, v33 = 0, v44 = 0, v55 = 0, v66 = 0, v77 = 0, v88 = 0, v99 = 0,

        lastDigit = 0, sum = 0, reg = /^\d+$/;
        var taxno = this.val();
        if (!taxno.match(reg)) {
            return false;
        }
        if (taxno.length !== 10) {
            return false;
        }
        /*
          Vergi Kimlik Numarası nın ilk dokuz karakteri yan yana yazılır. 
          Sağdan başlamak üzere altına artan sırada 1’den 9 a kadar rakam yazılır. 
         */
        v1 = (Number(taxno.charAt(0)) + 9) % 10;
        v2 = (Number(taxno.charAt(1)) + 8) % 10;
        v3 = (Number(taxno.charAt(2)) + 7) % 10;
        v4 = (Number(taxno.charAt(3)) + 6) % 10;
        v5 = (Number(taxno.charAt(4)) + 5) % 10;
        v6 = (Number(taxno.charAt(5)) + 4) % 10;
        v7 = (Number(taxno.charAt(6)) + 3) % 10;
        v8 = (Number(taxno.charAt(7)) + 2) % 10;
        v9 = (Number(taxno.charAt(8)) + 1) % 10;
        lastDigit = Number(taxno.charAt(9));

        /*  
        Elde edilen sayının altına sağdan itibaren 2’den başlayıp 2’nin katları 512’ye kadar yazılır.
        Bundan sonra bu iki sayı kolon bazında çarpılarak, her kolonun altına bulunan sayı yazılır.
        */
        v11 = (v1 * 512) % 9;
        v22 = (v2 * 256) % 9;
        v33 = (v3 * 128) % 9;
        v44 = (v4 * 64) % 9;
        v55 = (v5 * 32) % 9;
        v66 = (v6 * 16) % 9;
        v77 = (v7 * 8) % 9;
        v88 = (v8 * 4) % 9;
        v99 = (v9 * 2) % 9;

        /*
        Sıfır harici rakamların mod 9 unun sonucu sıfır çıkarsa 9 a eşitlenir
        */
        if (v1 !== 0 && v11 === 0) { v11 = 9; }
        if (v2 !== 0 && v22 === 0) { v22 = 9; }
        if (v3 !== 0 && v33 === 0) { v33 = 9; }
        if (v4 !== 0 && v44 === 0) { v44 = 9; }
        if (v5 !== 0 && v55 === 0) { v55 = 9; }
        if (v6 !== 0 && v66 === 0) { v66 = 9; }
        if (v7 !== 0 && v77 === 0) { v77 = 9; }
        if (v8 !== 0 && v88 === 0) { v88 = 9; }
        if (v9 !== 0 && v99 === 0) { v99 = 9; }

        /*
         Mutlak değerler toplamı sonucunda elde ettiğimiz sayılar yan yana toplanarak çıkan sonuç, 
         sonu sıfır olan bir üst tam sayıdan çıkarılır. 
         Elde edilen rakam CHECK DIGIT rakamıdır. 
         Mutlak değerler toplamı elde edilen sayının sonu sıfır ise check digit rakamı sıfır olarak yazılır.
         */
        sum = v11 + v22 + v33 + v44 + v55 + v66 + v77 + v88 + v99;
        sum = (sum % 10 === 0) ? 0 : (10 - (sum % 10));
        return (sum === lastDigit) ? true : false;
    };
})(jQuery);