﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SeturMarinasWeb.Web.Helpers
{
    public class KsRequiredAttributeAdapter : RequiredAttributeAdapter
    {
        public KsRequiredAttributeAdapter(ModelMetadata metadata, ControllerContext context, RequiredAttribute attribute)
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessageResourceType == null)
            {
                attribute.ErrorMessageResourceType = typeof(global::Resources.ValidationEmbed);
            }

            if (attribute.ErrorMessageResourceName == null)
            {
                attribute.ErrorMessageResourceName = "PropertyValueRequired";
            }
        }
    }
}