﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class MailHelper
    {
        public string Address { get; set; }
        public string DisplayName { get; set; }
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; }
        public string Body { get; set; }
        private List<string> ToList { get; set; }
        private List<string> BccList { get; set; }
        private List<string> CcList { get; set; }
        private List<Attachment> AttachmentList { get; set; }

        public void AddTo(params string[] to)
        {
            foreach (var t in to)
            {
                ToList.Add(t);
            }
        }

        public void AddBcc(params string[] to)
        {
            if (BccList == null)
            {
                BccList = new List<string>();
            }
            foreach (var t in to)
            {
                BccList.Add(t);
            }
        }

        public void AddCc(params string[] to)
        {
            if (CcList == null)
            {
                CcList = new List<string>();
            }
            foreach (var t in to)
            {
                CcList.Add(t);
            }
        }

        public void AddAttachment(params Attachment[] attachments)
        {
            if (AttachmentList == null)
            {
                AttachmentList = new List<Attachment>();
            }
            foreach (var t in attachments)
            {
                AttachmentList.Add(t);
            }
        }

        public MailHelper()
        {
            ToList = new List<string>();
            IsBodyHtml = true;
        }

        public bool Send(string host = null, int? port = null, bool throwException = false)
        {
            try
            {
                using (SmtpClient mailClient = GetClient(host, port))
                {

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(DisplayName, DisplayName);
                        mail.Sender = new MailAddress(DisplayName, DisplayName);
                        mail.SubjectEncoding = System.Text.Encoding.UTF8;
                        mail.BodyEncoding = System.Text.Encoding.UTF8;
                        mail.Subject = Subject;
                        mail.IsBodyHtml = IsBodyHtml;
                        mail.Body = Body;

                        foreach (var to in ToList)
                        {
                            mail.To.Add(to);
                        }

                        if (CcList != null)
                        {
                            foreach (var cc in CcList)
                            {
                                mail.CC.Add(cc);
                            }
                        }

                        if (BccList != null)
                        {
                            foreach (var bcc in BccList)
                            {
                                mail.Bcc.Add(bcc);
                            }
                        }

                        if (AttachmentList != null)
                        {
                            foreach (var attachment in AttachmentList)
                            {
                                mail.Attachments.Add(attachment);
                            }
                        }

                        mailClient.Send(mail);
                        return true;

                    }
                }
            }
            catch (Exception ex)
            {
                if (throwException)
                {
                    throw ex;
                }
                return false;

            }
        }

        private SmtpClient GetClient(string host = null, int? port = null)
        {
            if (host != null)
            {
                if (port.HasValue)
                {
                    return new SmtpClient(host, port.Value);
                }
                return new SmtpClient(host);
            }
            return new SmtpClient();
        }

        public async Task SendAsync()
        {
            using (SmtpClient mailClient = new SmtpClient())
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(Address, DisplayName);
                    mail.Sender = new MailAddress(Address, DisplayName);
                    mail.SubjectEncoding = Encoding.GetEncoding("ISO-8859-9");
                    mail.BodyEncoding = Encoding.GetEncoding("Windows-1254");
                    mail.Subject = Subject;
                    mail.IsBodyHtml = IsBodyHtml;
                    mail.Body = Body;

                    foreach (var to in ToList)
                    {
                        mail.To.Add(to);
                    }

                    if (CcList != null)
                    {
                        foreach (var cc in CcList)
                        {
                            mail.CC.Add(cc);
                        }
                    }

                    if (BccList != null)
                    {
                        foreach (var bcc in BccList)
                        {
                            mail.Bcc.Add(bcc);
                        }
                    }
                    if (AttachmentList != null)
                    {
                        foreach (var attachment in AttachmentList)
                        {
                            mail.Attachments.Add(attachment);
                        }
                    }
                    await mailClient.SendMailAsync(mail);
                }
            }
        }

        public void Clear()
        {
            Address = string.Empty;
            DisplayName = string.Empty;
            Subject = string.Empty;
            IsBodyHtml = true;
            if (ToList != null)
            {
                ToList.Clear();
            }
            if (BccList != null)
            {
                BccList.Clear();
            }
            if (CcList != null)
            {
                CcList.Clear();
            }
            if (AttachmentList != null)
            {
                AttachmentList.Clear();
            }
            IsBodyHtml = true;
            Body = string.Empty;
        }
    }
}
