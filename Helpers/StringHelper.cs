﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace Ks.Utility.Helper
{
    /// <summary>
    /// Some basic string functions
    /// </summary>
    public class StringHelper
    {
        #region Methods

        public static string StreamToString(Stream str)
        {
            string retval = string.Empty;
            if (str != null)
            {
                StreamReader sr = new StreamReader(str);
                retval = sr.ReadToEnd();
            }
            return retval;
        }

        /// <summary>
        /// Capitalizes input string. Converts the first character to uppercase,
        /// the remainings to lowercase.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public static string Capitalize(string input, CultureInfo cultureInfo)
        {
            if (!string.IsNullOrEmpty(input))
            {
                char firstChar = input[0];
                firstChar = char.ToUpper(firstChar, cultureInfo);
                string remainingPart = string.Empty;
                if (input.Length > 1)
                {
                    remainingPart = input.Substring(1, input.Length - 1);
                    remainingPart = remainingPart.ToLower(cultureInfo);
                }
                return firstChar + remainingPart;
            }

            return string.Empty;
        }

        /// <summary>
        /// Allows HTTP and FTP URL's, domain name must start with alphanumeric and can contain a port number
        /// followed by a path containing a standard path character and ending in common file suffixies found in URL's
        /// and accounting for potential CGI GET data
        /// </summary>
        /// <param name="strUrl">String to check</param>
        /// <returns>True if valid else false</returns>
        public static bool IsValidUrl(string strUrl)
        {
            const string regExPattern = @"^^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_=]*)?$";
            return MatchString(strUrl, regExPattern);
        }

        /// <summary>
        /// Allows common email address that can start with a alphanumeric char and contain word, dash and period characters
        /// followed by a domain name meeting the same criteria followed by a alpha suffix between 2 and 9 character lone
        /// </summary>
        /// <param name="strEmail"></param>
        /// <returns></returns>
        public static bool IsValidEmailAddress(string strEmail)
        {
            const string regExPattern = "^((\\\"[^\\\"\\f\\n\\r\\t\\v\\b]+\\\")|([\\w\\!\\#\\$\\%\\&\\'" +
                                        "\\*\\+\\-\\~\\/\\^\\`\\|\\{\\}]+(\\.[\\w\\!\\#\\$\\%\\&\\'\\*" +
                                        "\\+\\-\\~\\/\\^\\`\\|\\{\\}]+)*))@((\\[(((25[0-5])|(2[0-4][0" +
                                        "-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?" +
                                        "[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])" +
                                        ")\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\\])|(((25" +
                                        "[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-" +
                                        "4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0" +
                                        "-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0" +
                                        "-9])))|((([A-Za-z0-9\\-])+\\.)+[A-Za-z\\-]+))$";
            //string regExPattern = @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$";
            return MatchString(strEmail, regExPattern);
        }

        /// <summary>
        /// Decomposed method which actually creates the pattern object and determines the match.
        /// Used by all of the other functions. 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="regexstr"></param>
        /// <returns></returns>
        static bool MatchString(string str, string regexstr)
        {
            str = str.Trim();
            Regex pattern = new Regex(regexstr);
            return pattern.IsMatch(str);
        }

        public static byte[] BinarySerialize(object value)
        {
            byte[] retval = null;
            if (value != null)
            {
                try
                {
                    BinaryFormatter binformatter = new BinaryFormatter();
                    MemoryStream ms = new MemoryStream();
                    binformatter.Serialize(ms, value);
                    retval = ms.ToArray();
                }
                catch
                {
                    retval = null;
                }

            }
            return retval;
        }

        /// <summary>
        /// Serializes an object to XML
        /// </summary>
        /// <param name="value">Object to serialize</param>
        /// <returns>Serialized xml</returns>
        public static string Serialize(object value)
        {
            string retval;
            if (value != null)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(value.GetType());
                    StringWriter writer = new StringWriter();
                    serializer.Serialize(writer, value, null);
                    retval = writer.ToString();
                }
                catch
                {
                    retval = string.Empty;
                }
            }
            else
            {
                retval = string.Empty;
            }
            return retval;
        }

        public static object Deserialize(byte[] byteArray)
        {
            BinaryFormatter binformatter = new BinaryFormatter();
            object retval = null;
            if (byteArray != null)
            {
                MemoryStream ms = new MemoryStream(byteArray);
                try
                {
                    retval = binformatter.Deserialize(ms);
                }
                catch (Exception exp)
                {
                    throw new Exception("Can't Deserialize Binarydata ", exp);
                }

            }
            return retval;
        }

        /// <summary>
        /// Deserializes an xml string to object
        /// </summary>
        /// <param name="xmlString">Xml string to deserialize</param>
        /// <param name="type">Type of object to create an instance of</param>
        /// <returns>Deserialized object instance</returns>
        public static object Deserialize(string xmlString, Type type)
        {
            if (string.IsNullOrEmpty(xmlString))
                return null;
            XmlSerializer serializer = new XmlSerializer(type);
            StringReader reader = new StringReader(xmlString);
            XmlReader xmlReader = XmlReader.Create(reader);
            if (!serializer.CanDeserialize(xmlReader)) return null;
            object retVal = serializer.Deserialize(xmlReader);
            return retVal;
        }

        public static string ConvertObjectToString(object obj)
        {
            return obj?.ToString() ?? string.Empty;
        }

        public static string GetValueBetweenTags(string text, string tagKey)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            string startTag = string.Format("||{0}||", tagKey);
            string endTag = string.Format("|||{0}||", tagKey);

            var startTagIdx = text.IndexOf(startTag, 0, StringComparison.InvariantCulture);

            if (startTagIdx < 0)
            {
                return string.Empty;
            }

            var endTagIdx = text.IndexOf(endTag, startTagIdx + startTag.Length, StringComparison.InvariantCulture);

            if (endTagIdx > 0)
            {
                return text.Substring(startTagIdx + startTag.Length, endTagIdx - startTagIdx - startTag.Length);
            }

            return string.Empty;
        }

        #endregion Methods

        public static string ToCapitalizeFirstLetter(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;

            var cultureInfo = Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(str);
        }

        public static string Strip(string text, int length = 0)
        {
            if (string.IsNullOrWhiteSpace(text))
                return "";
            text = Regex.Replace(text, "<.*?>", string.Empty, RegexOptions.Multiline | RegexOptions.Compiled);
            text = RemoveSpacesToOne(text);
            if (length > 0)
            {
                text = Substring(text, length);
            }
            return text;
        }

        public static string ToAlphaNumeric(string text, string toChar = "")
        {
            if (string.IsNullOrWhiteSpace(text))
                return "";
            return Regex.Replace(text, "[^a-zA-Z0-9-_ çöıişüğÇÖŞİIĞÜ]", toChar, RegexOptions.Multiline | RegexOptions.Compiled);
        }

        public static string ToSearchString(string text, string toChar = "")
        {
            if (string.IsNullOrWhiteSpace(text))
                return "";
            return Regex.Replace(text, "[^a-zA-Z-_ çöıişüğÇÖŞİIĞÜ]", toChar, RegexOptions.Multiline | RegexOptions.Compiled);
        }

        public static string ToSearchWildCardString(string text, string toChar = "")
        {
            return Regex.Replace(text, "[çöıişüğcosugÇÖŞİIĞÜCOSUG]", toChar, RegexOptions.Multiline | RegexOptions.Compiled);
        }
        public static string ToSearchRexep(string text)
        {
            text = Regex.Replace(text, "[ıi]", "[ıi]", RegexOptions.Multiline | RegexOptions.Compiled);
            text = Regex.Replace(text, "[oö]", "[oö]", RegexOptions.Multiline | RegexOptions.Compiled);
            text = Regex.Replace(text, "[cç]", "[cç]", RegexOptions.Multiline | RegexOptions.Compiled);
            text = Regex.Replace(text, "[uü]", "[uü]", RegexOptions.Multiline | RegexOptions.Compiled);
            text = Regex.Replace(text, "[gğ]", "[gğ]", RegexOptions.Multiline | RegexOptions.Compiled);
            return text;
        }
        public static string ToEnglishLetters(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return "";
            text = text.ToLower(new System.Globalization.CultureInfo("tr-TR"));
            text = text.Replace('ç', 'c');
            text = text.Replace('ö', 'o');
            text = text.Replace('ş', 's');
            text = text.Replace('ı', 'i');
            text = text.Replace('ğ', 'g');
            text = text.Replace('ü', 'u');
            return text;
        }
        public static string RemoveSpacesToOne(string text)
        {
            return Regex.Replace(text, @"\s{2,}", " ");
        }

        public static string RemoveSpecialChars(string text)
        {
            return Regex.Replace(text, @"[\s\r\n\t]+", " ");
        }

        public static string HtmlEncode(string text)
        {
            return HttpUtility.HtmlAttributeEncode(text);
        }

        public static string HtmlDecode(string text)
        {
            return HttpUtility.HtmlDecode(text);
        }

        public static string Substring(string text, int length)
        {
            return Substring(text, length, false);
        }

        public static string Substring(string text, int length, bool ellipsis)
        {
            if (string.IsNullOrEmpty(text) || text.Length <= length)
            {
                return text;
            }
            if (!ellipsis)
            {
                return text.Substring(0, length);
            }

            text = text.Substring(0, length - 3);
            int lastEmptyIndex = text.LastIndexOf(' ');
            return lastEmptyIndex > 0 ? text.Substring(0, lastEmptyIndex) + "..." : text + "...";
        }

        public static string SubString(this string text, int length, bool ellipsis)
        {
            return Substring(text, length, ellipsis);
        }

        public static T Deserialize<T>(string input)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Deserialize<T>(input);
        }

        public static string Serialize(object input)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(input);
        }

        public static string CapitalizeEachWord(string input)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(input))
                    return string.Empty;

                string[] nameParts = input.Split(' ');
                for (int i = 0; i < nameParts.Length; i++)
                {
                    nameParts[i] = nameParts[i].Substring(0, 1).ToUpperInvariant() + nameParts[i].Substring(1);
                }
                return string.Join(" ", nameParts);
            }
            catch
            {

            }
            return input;
        }

        public static string FixUtfEncoding(string txt, Encoding fromEncoding = null)
        {
            if (string.IsNullOrWhiteSpace(txt))
                return "";
            txt = txt.Replace("Â", "");
            if (fromEncoding == null)
                fromEncoding = Encoding.GetEncoding("iso-8859-9");
            if ("çöşğüÇÖŞİĞÜı".Select(c => Encoding.Default.GetString(Encoding.UTF8.GetBytes(c.ToString()))).Any(txt.Contains))
            {
                return Encoding.UTF8.GetString(fromEncoding.GetBytes(txt));
            }
            return txt;
        }

        public static string ToJson(object data)
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            return JsonConvert.SerializeObject(data, Formatting.Indented, jsonSerializerSettings);
        }

        public static List<string> GetWords(params string[] sentences)
        {
            List<string> words = new List<string>();
            Regex wordRegex = new Regex(@"[\w']+");
            if (sentences == null)
                return words;

            foreach (string sentenceItem in sentences)
            {
                if (string.IsNullOrEmpty(sentenceItem))
                    continue;

                string input = sentenceItem.ToLower();
                int startIndex = 0;

                while (true)
                {
                    Match match = wordRegex.Match(input, startIndex);
                    if (!match.Success)
                    {
                        break;
                    }

                    string word = input.Substring(match.Index, match.Length);
                    if (!words.Contains(word))
                    {
                        words.Add(word);
                    }

                    startIndex = match.Index + match.Length;
                }
            }

            return words;
        }

        public static string ToUrl(this string str)
        {
            string rtr = str;
            rtr = rtr.Replace("http://maps.google.com", "@@@@@1");
            Regex urlRegex = new Regex("(http:\\/\\/([\\w.]+\\/?)\\S*)", RegexOptions.IgnoreCase);
            rtr = urlRegex.Replace(rtr, "<a href=\"$1\" target=\"_blank\">$1</a>");
            Regex emailregex = new Regex("([a-zA-Z_0-9.-]+\\@[a-zA-Z_0-9.-]+\\.\\w+)");
            rtr = emailregex.Replace(rtr, "<a href=\"mailto:$1\">$1</a>");
            rtr = rtr.Replace("@@@@@1", "http://maps.google.com");
            return rtr;
        }

        public static bool HasAnyAbusiveWord(string content)
        {
            try
            {
                var abusiveWords = GetAbusiveWords();

                string CensoredText = "[=HASABUSIVEWORD=]";
                string PatternTemplate = @"\b({0})(s?)\b";
                RegexOptions Options = RegexOptions.IgnoreCase;

                IEnumerable<Regex> badWordMatchers = abusiveWords.Select(x => new Regex(string.Format(PatternTemplate, Regex.Escape(x)), Options));
                string output = badWordMatchers.Aggregate(content, (current, matcher) => matcher.Replace(current, CensoredText));

                return output.Contains(CensoredText);
            }
            catch (Exception ex)
            {
                //LogHelper.Error(ex);
                return false;
            }
        }

        private static string[] GetAbusiveWords()
        {
            try
            {
                string filePath = HttpContext.Current.Server.MapPath(string.Format("/Assets/files/{0}", SystemSettings.FileConfiguration.AbusiveWordsFileName));
                //string filePath = HttpContext.Current.Server.MapPath(string.Format("/Assets/files/{0}", "abusive_words_20150203.txt"));
                string[] abusiveWords = File.ReadAllLines(filePath);
                return abusiveWords;
            }
            catch (Exception ex)
            {
                //LogHelper.Error(ex);
            }

            return new string[0];
        }



        public static int GetMonths(string month)
        {
            int ay = 1;
            switch (month)
            {
                case "ocak":
                    ay = 1;
                    break;

                case "subat":
                    ay = 2;
                    break;
                case "mart":
                    ay = 3;
                    break;
                case "nisan":
                    ay = 4;
                    break;
                case "mayis":
                    ay = 5;
                    break;
                case "haziran":
                    ay = 6;
                    break;
                case "temmuz":
                    ay = 7;
                    break;
                case "agustos":
                    ay = 8;
                    break;
                case "eylul":
                    ay = 9;
                    break;
                case "ekim":
                    ay = 10;
                    break;
                case "kasim":
                    ay = 11;
                    break;
                case "aralik":
                    ay = 12;
                    break;

            }
            return ay;
        }

        public static string ToMoneyString(this object obj)
        {
            if (obj == null) return string.Empty;

            return string.Format("{0:N2}", obj);
        }

        public static string ToMoneyString(this object obj, int righDigit)
        {
            if (obj == null) return string.Empty;
            if (righDigit <= 0 || righDigit > 6) righDigit = 2;

            string format = "{0:N" + righDigit + "}";
            return string.Format(format, obj);
        }

        public static string ToMoneyString(this object obj, string format)
        {
            if (obj == null) return string.Empty;

            return string.Format(format, obj);
            //return string.Format("{0:#.00}", obj);
        }

        public static string ToDayMonthFormat(this DateTime obj)
        {
            string result = string.Empty;
            if (obj.Day < 10)
            {
                result += "0" + obj.Day;
            }
            else
            {
                result += obj.Day;
            }
            result += "/";
            if (obj.Month < 10)
            {
                result += "0" + obj.Month;
            }
            else
            {
                result += obj.Month;
            }

            return result;
        }
    }
}