﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;

namespace Common.Helpers
{
    public class AdModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string AccountName { get; set; }
        public string Cn { get; set; }
    }

    public static class ActiveDirectoryHelper
    {
        /// <summary>
        /// Ad kullanıcı bilgilerini getirir
        /// </summary>
        /// <param name="connectionLdap">Ldap connection string</param>
        /// <returns></returns>
        public static List<AdModel> GetAdUsers(string connectionLdap)
        {
            // Ad'den çekmek istediğimiz property'ler
            var fields = new List<string>() {"mail", "sAMAccountName", "displayName", "givenName", "sn", "distinguishedName","cn" };
            var viewList = new List<AdModel>();

            try
            {
                DirectoryEntry searchRoot = new DirectoryEntry(connectionLdap);
                var search = new DirectorySearcher(searchRoot)
                {
                    Filter = "(&(objectClass=user)(objectCategory=person))"
                };

                foreach (var field in fields)
                    search.PropertiesToLoad.Add(field);

                var resultCol = search.FindAll();

                for (int counter = 0; counter < resultCol.Count; counter++)
                {
                    var result = resultCol[counter];
                    try
                    {
                        if (result.Properties["mail"].Count != 0)
                        {
                            var view = new AdModel {Email = (String) result.Properties["mail"][0]};

                            if (result.Properties["cn"].Count != 0)
                            {
                                view.Cn = (String)result.Properties["cn"][0];
                            }

                            if (result.Properties["givenName"].Count != 0)
                            {
                                view.Name = (String)result.Properties["givenName"][0];
                            }

                            if (result.Properties["sn"].Count != 0)
                            {
                                view.Surname = (String)result.Properties["sn"][0];
                            }

                            if (result.Properties["displayName"].Count != 0)
                            {
                                view.FullName = (String)result.Properties["displayName"][0];
                            }

                            if (result.Properties["sAMAccountName"].Count != 0)
                            {
                                view.AccountName = (String)result.Properties["sAMAccountName"][0];
                            }

                            viewList.Add(view);
                        }
                    }
                    catch (Exception ex)
                    {
                        //
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }

            return viewList;
        }
    }
}
