﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Common.Helpers
{
    public static class RegexHelper
    {
        public static bool IsValidCreditCardNumber(string cardNumber)
        {
            if (cardNumber.Any(c => !char.IsDigit(c)))
            {
                return false;
            }

            var checksum = cardNumber
                .Select((c, i) => (c - '0') << ((cardNumber.Length - i - 1) & 1))
                .Sum(n => n > 9 ? n - 9 : n);

            return (checksum % 10) == 0 && checksum > 0;
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            return Regex.IsMatch(email,
                "^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*\\.([a-z]{2,7})$",
                RegexOptions.IgnoreCase);
        }
    }
}
