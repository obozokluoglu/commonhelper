﻿using ClosedXML.Excel;
using Common.Extensions;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Common.Helpers
{
    public class ExcelExport
    {
        public void Dispose()
        {

        }

        public string Export(DataTable dataSource, string filePath, List<string> headers = null, List<string> orderedColumnNames = null)
        {
            var workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var columns = orderedColumnNames != null ? orderedColumnNames : dataSource.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();

            #region Styles
            var altRowStyle = workbook.CreateCellStyle();
            altRowStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            altRowStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;

            var columnStyle = workbook.CreateCellStyle();
            columnStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Medium;
            columnStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Medium;
            columnStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Medium;
            columnStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;

            var colFont = workbook.CreateFont();
            colFont.FontHeightInPoints = 12;
            colFont.FontName = "Calibri";
            colFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

            columnStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.PaleBlue.Index;
            columnStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
            #endregion

            for (int i = 0; i < columns.Count; i++)
            {
                sheet.SetColumnWidth(i, 5000);

                var cell = headerRow.CreateCell(i);

                cell.CellStyle = columnStyle;
                cell.CellStyle.SetFont(colFont);

                cell.SetCellValue(headers != null && headers.Count > i ? headers[i] : columns[i]);
            }
            sheet.CreateFreezePane(0, 1, 0, 1);

            int rowNumber = 1;

            foreach (DataRow dr in dataSource.Rows)
            {
                var row = sheet.CreateRow(rowNumber++);

                for (int i = 0; i < columns.Count; i++)
                {
                    var col = dataSource.Columns[columns[i]];
                    var cell = row.CreateCell(i);

                    if (col != null)
                    {
                        if (col.DataType == typeof(DateTime))
                        {
                            if (dr[col] != DBNull.Value && dr[col] != null)
                            {
                                var val = Convert.ToDateTime(dr[col]);
                                cell.SetCellValue(val.ToString("dd.MM.yyyy HH:mm"));
                            }
                        }
                        else if (col.DataType == typeof(bool))
                        {
                            if (dr[col] != DBNull.Value && dr[col] != null)
                            {
                                var val = Convert.ToBoolean(dr[col]);
                                cell.SetCellValue(val ? "✔" : "✘");
                            }
                        }
                        else
                        {
                            cell.SetCellValue(dr[col].ToStringWithNullControl());
                        }
                    }

                    if (rowNumber % 2 == 1)
                    {
                        cell.CellStyle = altRowStyle;
                    }
                }
            }

            sheet.SetAutoFilter(new CellRangeAddress(0, rowNumber - 1, 0, columns.Count - 1));


            if (!filePath.ToLower().EndsWith(".xlsx") && !filePath.ToLower().EndsWith(".xls"))
            {
                filePath += ".xlsx";
            }
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using (var fileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.ReadWrite))
            {
                workbook.Write(fileStream);
                return filePath;
            }
        }

        public void ExportWithTemplate(string templateFilePath, string exportFilePath, List<ExcelCell> values = null, List<ExcelTable> tables = null)
        {
            if (!File.Exists(templateFilePath))
            {
                throw new FileNotFoundException("Template file not found");
            }

            using (var doc = new SLDocument(templateFilePath))
            {
                if (values != null)
                {
                    foreach (var val in values.Where(v => v.Value != null))
                    {
                        if (val.CellKey.IsNullOrWhiteSpace())
                        {
                            doc.SetCellValue(val.RowIndex, val.ColumnIndex, val.Value);
                        }
                        else
                        {
                            doc.SetCellValue(val.CellKey, val.Value);
                        }
                    }
                }

                if (tables != null)
                {
                    tables = tables.Where(t => t.Table != null).ToList();

                    foreach (var table in tables)
                    {
                        doc.InsertRow(table.StartRowIndex, table.Table.Rows.Count);

                        var rowIndex = table.StartRowIndex;

                        foreach (DataRow row in table.Table.Rows)
                        {
                            var colIndex = table.StartColumnIndex;

                            for (int i = 0; i < table.Table.Columns.Count; i++)
                            {
                                doc.SetCellValue(rowIndex, colIndex, row[i].ToStringWithNullControl());
                                colIndex++;
                            }

                            rowIndex++;
                        }
                    }
                }

                doc.SaveAs(exportFilePath);
            }
        }

        public static IEnumerable<T> ConvertExcelDataToType<T>(Stream data, Dictionary<int, string> cellMappings, bool headerUsed = true) where T : new()
        {
            var workbook = new XLWorkbook(data);
            var workSheet = workbook.Worksheets.First();
            var range = workSheet.RangeUsed();
            var start = headerUsed ? 2 : 1;
            var end = range.RowCount() + 1;
            List<T> tList = new List<T>();
            var typeOfT = typeof(T);
            for (int i = start; i < end; i++)
            {
                var row = workSheet.Row(i);
                T t = new T();
                foreach (var cellMapping in cellMappings)
                {
                    var c = row.Cell(cellMapping.Key);
                    var prop = typeOfT.GetProperty(cellMapping.Value);
                    var type = prop.PropertyType;
                    if (type.Name == "Int32" && c.Value.ToString() == string.Empty)
                        c.Value = 0;
                    if (type.Name == "Boolean" && c.Value.ToString() == string.Empty)
                        c.Value = 0;
                    var value = Convert.ChangeType(c.Value, type);
                    prop.SetValue(t, value);
                }
                tList.Add(t);
            }
            return tList;
        }

        public static IEnumerable<T> ConvertExcelDataToType<T>(string path, Dictionary<int, string> cellMappings) where T : new()
        {
            var workbook = new XLWorkbook(path);
            var workSheet = workbook.Worksheets.First();
            List<T> tList = new List<T>();
            foreach (var row in workSheet.Rows())
            {
                T t = new T();
                var typeOfT = t.GetType();
                foreach (var cellMapping in cellMappings)
                {
                    var c = row.Cell(cellMapping.Key);
                    typeOfT.GetProperty(cellMapping.Value).SetValue(t, c.Value);
                }
                tList.Add(t);
            }
            return tList;
        }

        public static MemoryStream GetExcelAsMemoryStream(string path)
        {
            var workbook = new XLWorkbook(path);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            return stream;
        }

    }

    public class ExcelCell
    {
        public string CellKey { get; set; }
        public int RowIndex { get; set; }
        public int ColumnIndex { get; set; }
        public dynamic Value { get; set; }
    }

    public class ExcelTable
    {
        public int StartRowIndex { get; set; } = 1;
        public int StartColumnIndex { get; set; } = 1;
        public DataTable Table { get; set; }
    }
}
