﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Common.Helpers
{
    public static class GZipHelper
    {
        public static void CreateZipFile(string targetPath, Dictionary<string, Stream> zipContents)
        {
            using (var ms = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    foreach (var item in zipContents)
                    {
                        var xmlEntry = zipArchive.CreateEntry(item.Key, CompressionLevel.Fastest);
                        using (var entryStream = xmlEntry.Open())
                        {
                            item.Value.CopyTo(entryStream);
                        }
                    }
                    zipArchive.Dispose();
                }
                File.WriteAllBytes(targetPath, ms.ToArray());
                ms.Close();
                ms.Dispose();
            }
        }

        private static void CopyTo(Stream src, Stream dest)
        {
            var bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

        public static string Unzip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    CopyTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }

        public static byte[] Zip(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    CopyTo(msi, gs);
                }

                return mso.ToArray();
            }
        }
    }
}
