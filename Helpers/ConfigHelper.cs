﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KocSchool.Common.Helper
{
    public class ConfigHelper
    {

        /// <summary>
        /// Get String Data For AppSetting Value.
        /// </summary>
        /// <param name="key">AppSetting Key.</param>
        /// <returns>String Value.</returns>
        public static string GetStringData(string key)
        {
            if (ConfigurationManager.AppSettings[key] != null)
                return ConfigurationManager.AppSettings[key];
            else
                return string.Empty;
        }
        /// <summary>
        /// Get Bool Data For AppSetting Value.
        /// </summary>
        /// <param name="key">AppSetting Key.</param>
        /// <returns>Bool Value.</returns>
        public static bool GetBoolData(string key)
        {
            if (ConfigurationManager.AppSettings[key] != null)
                return bool.Parse(ConfigurationManager.AppSettings[key]);
            else
                return default(bool);
        }
        /// <summary>
        /// Get Int Data For AppSetting Value.
        /// </summary>
        /// <param name="key">AppSetting Key.</param>
        /// <returns>Int Value.</returns>
        public static int GetIntData(string key)
        {
            if (ConfigurationManager.AppSettings[key] != null)
                return int.Parse(ConfigurationManager.AppSettings[key]);
            else
                return default(int);
        }
        /// <summary>
        /// Get Data For AppSetting Value.
        /// </summary>
        /// <typeparam name="T">Return Type.</typeparam>
        /// <param name="key">AppSetting Key.</param>
        /// <returns>Value.</returns>
        public static T GetConfigData<T>(string key)
        {
            T result = default(T);

            if (ConfigurationManager.AppSettings[key] != null)
            {
                try
                {
                    result = (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
                }
                catch (Exception ex)
                {
                    throw new Exception("Ocean.Core.Web.ConfigHelper.GetConfigData", ex);
                }
            }

            return result;
        }


        /// <summary>
        /// Get Connection String Info Object that include Connection String and other properties.
        /// </summary>
        /// <param name="key">Connection String Key.</param>
        /// <returns>ConnectionStringInfo Object.</returns>
        public static ConnectionStringInfo GetConnectionString(string key)
        {
            ConnectionStringInfo result = null;

            if (ConfigurationManager.ConnectionStrings[key] != null)
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[key];

                result = new ConnectionStringInfo();
                result.ConnectionString = settings.ConnectionString;
                result.ProviderName = settings.ProviderName;
                result.Name = settings.Name;

            }

            return result;
        }

    }
}
