﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace KocSchool.Common.Helper
{
    /// <summary>
    /// EWS üzerinden mail göndermek için kullanılan helper sınıfı...ş
    /// Author: Ercan
    /// </summary>
    public class EmailHelper
    {
        public EmailHelper()
        {
            ToRecipients = new List<string>();
            FilePaths = new List<string>();
            IsHtml = true;
        }

        public List<string> ToRecipients { get; set; }
        public List<string> FilePaths { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public bool IsHtml { get; set; }

        public bool SendEmail()
        {
            bool flag = false;
            try
            {
                ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);

                service.Url = new Uri(@"https://mail.koc.k12.tr/EWS/Exchange.asmx");
                ServicePointManager.ServerCertificateValidationCallback =
                        delegate(Object obj, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
                        {
                            return true;
                        };

                service.Credentials = new WebCredentials(AppConfigHelper.ApplicationEmailUserName, AppConfigHelper.ApplicationEmailUserPw);
                //service.UseDefaultCredentials = true;

                EmailMessage email = new EmailMessage(service);
                email.From = new EmailAddress(AppConfigHelper.ApplicationEmail, "");

                //Kimlere Gidecek
                foreach (var item in ToRecipients)
                {
                    email.ToRecipients.Add(item);
                }

                //Eklenecek dosyaların adresleri
                foreach (var item in FilePaths)
                {
                    email.Attachments.AddFileAttachment(item);
                }

                email.Subject = this.Subject;
                email.Body = this.Body;

                email.Send();
                flag = true;
            }
            catch 
            {
            }

            return flag;

        }
    }
}
