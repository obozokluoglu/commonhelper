﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public static class AssemblyHelper
    {
        public static List<Assembly> GetAllAssemblies()
        {

            var assembly = Assembly.Load("SeturMarinasWeb.WindowsService");

            var assemblies = (from file in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory)
                              where Path.GetExtension(file) == ".dll"
                              select Assembly.LoadFrom(file)).ToList();

            assemblies.Add(assembly);

            return assemblies;
        }
    }
}
