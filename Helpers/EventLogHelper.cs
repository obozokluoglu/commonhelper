﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace Common.Helpers
{
    internal static class EventLogHelper
    {
        /// <summary>
        /// Creates a new log in event log for source "IntelSchedulerService"
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="entryType">Entry type such as Error or Warning etc.</param>
        public static void Create(string message, EventLogEntryType entryType)
        {
            try
            {
                string source = "Common CMS";
                EventLog eventLog = new EventLog();

                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, source);
                }

                message = $"{DateTime.Now.ToString(CultureInfo.InvariantCulture)} - {message}";

                eventLog.Source = source;
                eventLog.EnableRaisingEvents = true;
                eventLog.WriteEntry(message, entryType);
                eventLog.Close();
            }
            catch (Exception ex)
            {
                //
            }
        }
    }
}
