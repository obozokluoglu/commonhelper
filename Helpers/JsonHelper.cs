﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Common.Helpers
{
    public static class JsonHelper
    {
        public static string CamelCaseSerialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }
        
        public static T JsonDeserialize<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public static string JsonSerialize<T>(T model)
        {
            return JsonConvert.SerializeObject(model);
        }
    }
}
