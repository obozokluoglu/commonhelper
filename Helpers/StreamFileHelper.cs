﻿using System.IO;
using System.Web;

namespace Common.Helpers
{
    public static class StreamFileHelper
    {
        public static byte[] GetAllBytes(Stream stream)
        {
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
        public static byte[] GetAllBytes(string fileName)
        {
            using (FileStream fs = new FileStream(@fileName, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    long numBytes = new FileInfo(fileName).Length;
                    byte[] buff = br.ReadBytes((int)numBytes);
                    return buff;
                }
            }
        }

        public static void WriteFileDisk(Stream input, string path)
        {
            using (var stream = new MemoryStream())
            {
                input.CopyTo(stream);
                File.WriteAllBytes(path, stream.ToArray());
            }
        }

        public static void WriteFileDisk(byte[] buffer, string path)
        {
            File.WriteAllBytes(path, buffer);
        }

        public static void DeleteFile(string path)
        {
            File.Delete(path);
        }
        public static byte[] ConvertToBytes(HttpPostedFileBase file)
        {
            byte[] fileBytes = null;
            BinaryReader reader = new BinaryReader(file.InputStream);
            fileBytes = reader.ReadBytes((int)file.ContentLength);
            return fileBytes;
        }
    }
}
