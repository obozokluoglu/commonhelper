﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GastroClub.Data.Domain.Helpers
{
    public static class GeoHelper
    {
        public static double[] GetBoundingBox(double latitude, double longitude, int distanceInMeters)
        {

           double[] boundingBox = new double[4];

           double latRadian = ToRadians(latitude);

           double degLatKm = 110.574235;
           double degLongKm = 110.572833 * Math.Cos(latRadian);
           double deltaLat = distanceInMeters / 1000.0 / degLatKm;
           double deltaLong = distanceInMeters / 1000.0 / degLongKm;

           double minLat = latitude - deltaLat;
           double minLong = longitude - deltaLong;
           double maxLat = latitude + deltaLat;
           double maxLong = longitude + deltaLong;

            boundingBox[0] = minLat;
            boundingBox[1] = minLong;
            boundingBox[2] = maxLat;
            boundingBox[3] = maxLong;

            return boundingBox;
        }
        private static double ToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
    }
}
