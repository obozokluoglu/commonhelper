﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KocSchool.Common.Helper
{
    public class DateTimeHelper
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static IEnumerable<int> MonthDayNumbers
        {
            get
            {
                return
                    Enumerable.Range(1, 31);
            }
        }

        public static int GetMonthNumberFromName(string monthName)
        {
            int iMonthNo = Convert.ToDateTime("01-" + monthName + "-2011").Month;
            return iMonthNo;
        }

        public static string GetMonthNameFromNumber(int monthNumber)
        {
            DateTime dtDate = new DateTime(2000, monthNumber, 1);
            string sMonthFullName = dtDate.ToString("MMMM");
            return sMonthFullName;
        }

        public static string[] MonthNames
        {
            get
            {
                string[] monthNames = CultureInfo.GetCultureInfo("tr-Tr").DateTimeFormat.MonthNames;
                return monthNames.Take(12).ToArray();
            }
        }


        public static Dictionary<string, int> SeoFriendlyMonthNames
        {
            get
            {
                var dictionary = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
                dictionary.Add("Ocak", 1);
                dictionary.Add("Subat", 2);
                dictionary.Add("Mart", 3);
                dictionary.Add("Nisan", 4);
                dictionary.Add("Mayis", 5);
                dictionary.Add("Haziran", 6);
                dictionary.Add("Temmuz", 7);
                dictionary.Add("Agustos", 8);
                dictionary.Add("Eylul", 9);
                dictionary.Add("Ekim", 10);
                dictionary.Add("Kasim", 11);
                dictionary.Add("Aralik", 12);
                return dictionary;
            }
        }

        public static int GetMonthNumberFromSeoFriendlyMonthName(string monthName)
        {
            if (SeoFriendlyMonthNames.ContainsKey(monthName))
            {
                var month =
                    SeoFriendlyMonthNames.FirstOrDefault(
                        c => c.Key.Equals(monthName, StringComparison.OrdinalIgnoreCase));

                return month.Value;
            }

            return 0;
        }

        public static DateTime StartOfWeek(this DateTime dt)
        {
            int diff = dt.DayOfWeek - DayOfWeek.Monday;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static bool IsValidDate(string val, out DateTime dt)
        {
            bool valid = DateTime.TryParseExact(val, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            return valid;
        }


        public static DateTime GetFirstDayOfWeek()
        {
            DateTime dt = DateTime.Now;
            int diff = dt.DayOfWeek - DayOfWeek.Monday;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static string GetPrettyDateTime(DateTime dateTime)
        {
            return string.Format("{0}-{1}-{2}", dateTime.Day, dateTime.TurkishMonthName().ConvertTextForReWrite(),
                dateTime.Year);
        }

    }
}
