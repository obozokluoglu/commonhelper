﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KocSchool.Common.Helper
{
    public static class CharHelper
    {
        public static string RemoveInvalidChars(this string value)
        {
            return value
                .Replace("<", "")
                .Replace(">", "")
                .Replace("&", "")
                .Replace("'", "")
                .Replace("_", "")
                .Replace(".", "")
                .Replace("-", "")
                .Replace("?", "")
                .Replace("!", "")
                .Replace("@", "")
                .Replace(" ", "")
                .Replace("'", "");
        }

        public static string RemoveTrChars(this string value)
        {
            value = value.RemoveInvalidChars().ToLower();
            value = value.ToLower();
            return value
                .Replace(" ", "")
                .Replace("Ş", "S")
                .Replace("Ç", "C")
                .Replace("Ö", "O")
                .Replace("Ü", "U")
                .Replace("İ", "I")
                .Replace("Ğ", "G")
                .Replace("ş", "s")
                .Replace("ö", "o")
                .Replace("ü", "u")
                .Replace("ı", "i")
                .Replace("ğ", "g")
                .Replace("ç", "c");
        }
    }
}
